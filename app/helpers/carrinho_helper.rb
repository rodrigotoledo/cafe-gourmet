module CarrinhoHelper
  def sub_total_compra
    carrinho_usuario.sum{|t| t[:valor] * t[:qtd]}
  end

  def taxa_embalagem
    carrinho_usuario.sum{|t| t[:qtd] * Parametro.first_or_create.taxa_embalagem}
  end

  def total_compra
    sub_total_compra + taxa_embalagem
  end

end
