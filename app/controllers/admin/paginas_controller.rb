class Admin::PaginasController < Admin::AdminController
	uses_tiny_mce(:options => AppConfig.default_mce_options, :only => [:new, :edit])

  # GET /admin/paginas
  # GET /admin/paginas.xml
  def index
    @admin_paginas = Pagina.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @admin_paginas }
    end
  end

  # GET /admin/paginas/1
  # GET /admin/paginas/1.xml
  def show
    @admin_pagina = Pagina.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @admin_pagina }
    end
  end

  # GET /admin/paginas/new
  # GET /admin/paginas/new.xml
  def new
    @admin_pagina = Pagina.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @admin_pagina }
    end
  end

  # GET /admin/paginas/1/edit
  def edit
    @admin_pagina = Pagina.find(params[:id])
  end

  # POST /admin/paginas
  # POST /admin/paginas.xml
  def create
    @admin_pagina = Pagina.new(params[:pagina])

    respond_to do |format|
      if @admin_pagina.save
        # sleep 1
        format.html { redirect_to(@admin_pagina, :notice => 'Pagina foi criado(a) corretamente.') }
        format.xml  { render :xml => @admin_pagina, :status => :created, :location => @admin_pagina }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @admin_pagina.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /admin/paginas/1
  # PUT /admin/paginas/1.xml
  def update
    @admin_pagina = Pagina.find(params[:id])

    respond_to do |format|
      if @admin_pagina.update_attributes(params[:pagina])
        # sleep 1
        format.html { redirect_to(admin_paginas_path, :notice => 'Pagina foi atualizado(a) corretamente.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @admin_pagina.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/paginas/1
  # DELETE /admin/paginas/1.xml
  def destroy
    @admin_pagina = Pagina.find(params[:id])
    @admin_pagina.destroy

    respond_to do |format|
      # sleep 1
      format.html { redirect_to(admin_paginas_url) }
      format.xml  { head :ok }
    end
  end
end
