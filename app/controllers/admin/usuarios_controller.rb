class Admin::UsuariosController < Admin::AdminController
  # GET /admin/usuarios
  # GET /admin/usuarios.xml
  def index
    @admin_usuarios = Usuario.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @admin_usuarios }
    end
  end

  # GET /admin/usuarios/1
  # GET /admin/usuarios/1.xml
  def show
    @admin_usuario = Usuario.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @admin_usuario }
    end
  end

  # GET /admin/usuarios/new
  # GET /admin/usuarios/new.xml
  def new
    @admin_usuario = Usuario.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @admin_usuario }
    end
  end

  # GET /admin/usuarios/1/edit
  def edit
    @admin_usuario = Usuario.find(params[:id])
  end

  # POST /admin/usuarios
  # POST /admin/usuarios.xml
  def create
    @admin_usuario = Usuario.new(params[:admin_usuario])

    respond_to do |format|
      if @admin_usuario.save
        # sleep 1
        format.html { redirect_to(@admin_usuario, :notice => 'Usuario foi criado(a) corretamente.') }
        format.xml  { render :xml => @admin_usuario, :status => :created, :location => @admin_usuario }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @admin_usuario.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /admin/usuarios/1
  # PUT /admin/usuarios/1.xml
  def update
    @admin_usuario = Usuario.find(params[:id])

    respond_to do |format|
      if @admin_usuario.update_attributes(params[:admin_usuario])
        # sleep 1
        format.html { redirect_to(@admin_usuario, :notice => 'Usuario foi atualizado(a) corretamente.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @admin_usuario.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/usuarios/1
  # DELETE /admin/usuarios/1.xml
  def destroy
    @admin_usuario = Usuario.find(params[:id])
    @admin_usuario.destroy

    respond_to do |format|
      format.html { redirect_to(admin_usuarios_url) }
      format.xml  { head :ok }
    end
  end
end
