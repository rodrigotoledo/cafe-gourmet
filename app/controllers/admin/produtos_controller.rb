class Admin::ProdutosController < Admin::AdminController
  uses_tiny_mce(:options => AppConfig.default_mce_options, :only => [:new, :edit])
  
  # GET /admin/produtos
  # GET /admin/produtos.xml
  def index
    @admin_produtos = Produto.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @admin_produtos }
    end
  end

  # GET /admin/produtos/1
  # GET /admin/produtos/1.xml
  def show
    @admin_produto = Produto.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @admin_produto }
    end
  end

  # GET /admin/produtos/new
  # GET /admin/produtos/new.xml
  def new
    @admin_produto = Produto.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @admin_produto }
    end
  end

  # GET /admin/produtos/1/edit
  def edit
    @admin_produto = Produto.find(params[:id])
  end

  # POST /admin/produtos
  # POST /admin/produtos.xml
  def create
    @admin_produto = Produto.new(params[:produto])

    respond_to do |format|
      if @admin_produto.save
        # sleep 1
        format.html { redirect_to(admin_produtos_path, :notice => 'Produto foi criado(a) corretamente.') }
        format.xml  { render :xml => @admin_produto, :status => :created, :location => @admin_produto }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @admin_produto.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /admin/produtos/1
  # PUT /admin/produtos/1.xml
  def update
    @admin_produto = Produto.find(params[:id])

    respond_to do |format|
      if @admin_produto.update_attributes(params[:produto])
        # sleep 1
        format.html { redirect_to(admin_produtos_path, :notice => 'Produto foi atualizado(a) corretamente.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @admin_produto.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/produtos/1
  # DELETE /admin/produtos/1.xml
  def destroy
    @admin_produto = Produto.find(params[:id])
    @admin_produto.destroy

    respond_to do |format|
      # sleep 1
      format.html { redirect_to(admin_produtos_url) }
      format.xml  { head :ok }
    end
  end
end
