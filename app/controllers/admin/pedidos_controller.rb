class Admin::PedidosController < Admin::AdminController
  def index
  	@pedidos = Pedido.where('pagseguro_id IS NOT NULL').all
  end

  def show
    @pedido = Pedido.find(params[:id])
    render :layout => 'admin/simples'
  end

end
