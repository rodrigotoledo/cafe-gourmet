class Admin::ImagemProdutosController < Admin::AdminController

  # DELETE /admin/imagem_produtos/1
  # DELETE /admin/imagem_produtos/1.xml
  def destroy
    @admin_imagem_produto = ImagemProduto.find(params[:id])
    @admin_imagem_produto.destroy

    respond_to do |format|
      # sleep 1
      format.html { redirect_to(edit_admin_produto_path(params[:produto_id]),:notice=>'Imagem removida com sucesso') }
      format.xml  { head :ok }
    end
  end
end
