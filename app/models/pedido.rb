class Pedido < ActiveRecord::Base
	validates_presence_of :nome, :email, :cpf, :data_nascimento, :sexo, :telefone_1, :cep, :endereco, :numero, :bairro, :cidade, :estado,
  :entrega_cep, :entrega_endereco, :entrega_numero, :entrega_bairro, :entrega_cidade, :entrega_estado
  validates_presence_of :nome_fantasia, :razao_social, :cnpj, :telefone_empresa, :if => :pedido_para_empresa?


  def pedido_para_empresa?
    self.tipo_cliente == 1
  end
end
