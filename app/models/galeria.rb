class Galeria < ActiveRecord::Base
  has_many :imagem_galerias
  accepts_nested_attributes_for :imagem_galerias

  validates :nome, :presence => true
  validates :conteudo, :presence => true

  def capa
    self.imagem_galerias.first.img_galeria if self.imagem_galerias.count > 0
  end
end
