class ImagemGaleria < ActiveRecord::Base
  belongs_to :galeria

  has_attached_file :img_galeria, :styles => { 
    :normal => "260x260#", 
    :medium => "179x201#",
    :large => "400", 
    :thumb => "75x75#"}
    
  validates_attachment_presence :img_galeria
end
