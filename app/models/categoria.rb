class Categoria < ActiveRecord::Base
  alias_attribute :name, :nome
  validates :nome, :presence => true
end
