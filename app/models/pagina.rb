class Pagina < ActiveRecord::Base
	validates :titulo, :presence => true, :uniqueness => true
	validates :conteudo, :presence => true
	
	before_save :set_url
	
	def set_url
		self.url = self.titulo.parameterize
		true
	end
end
