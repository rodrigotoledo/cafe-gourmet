class ImagemProduto < ActiveRecord::Base
  belongs_to :produto
  has_attached_file :arquivo, :styles => { 
    :large => "260x260", 
    :medium => "179x201",
    :thumb => "75x75#"}
    
  validates_attachment_presence :arquivo
end
