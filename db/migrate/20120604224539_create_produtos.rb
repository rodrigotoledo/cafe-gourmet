class CreateProdutos < ActiveRecord::Migration
  def self.up
    create_table :produtos do |t|
      t.references :categoria
      t.string :nome
      t.string :codigo_interno
      t.text :descricao
      t.string :video
      t.has_attached_file :capa
      t.float :valor
      t.float :valor_promocional
      t.boolean :ativo, :default => true
      t.boolean :destaque_inicial

      t.timestamps
    end
  end

  def self.down
    drop_table :produtos
  end
end
