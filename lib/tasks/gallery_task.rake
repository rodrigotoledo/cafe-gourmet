namespace :gallery do
  task :fix_image_front => :environment do
    Gallery.all.each do |gallery|
      gallery_image = gallery.gallery_image || gallery.gallery_images.first
      next if gallery_image.nil?
      gallery.image_front = File.open(gallery_image.image.path)
      gallery.save
      # gallery.image_front = File.open()
    end
  end
end