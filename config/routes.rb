Agitosemana::Application.routes.draw do
  get "pedidos/index"

  match 'salvar-notificacao' => "notificacoes#salvar", :via => :post

  get "galerias/index"

  get "pagamento/retorno"

  match 'dados-cliente' => 'carrinho#dados_cliente', :as => :dados_cliente
  match 'carrinho' => 'carrinho#index', :as => :carrinho
  match 'carrinho/:produto_id' => 'carrinho#adicionar', :as => :adicionar_carrinho
  match 'carrinho/:produto_id/diminuir' => 'carrinho#diminuir', :as => :diminuir_carrinho
  match 'carrinho/:produto_id/remover' => 'carrinho#remover', :as => :remover_carrinho
  match 'efetuar_pagamento' => 'carrinho#efetuar_pagamento', :as => :efetuar_pagamento, :via => :post
  match 'finalizar' => 'finalizar#index', :as => :finalizar
  
  match 'produtos' => 'produtos#index', :as => :produtos
  match 'produto/:id' => 'produtos#detalhes', :as => :produto

  match 'fale-conosco' => 'fale_conosco#index', :as => :fale_conosco
  match 'enviar-fale-conosco' => 'fale_conosco#enviar_contato', :as => :enviar_contato, :via => :post

  match 'institucional/:pagina_url' => 'paginas#index', :as => :pagina
	match 'galeria/:id' => 'galerias#index', :as => :galeria
  
  

  namespace :admin do
    root :to => "produtos#index"
    
    match 'login/index'               => 'login#index',   :as => :login
    match 'login/logout'              => 'login#logout',  :as => :logout
    match 'login/enter'               => 'login#enter',   :as => :login_enter, :via => :post
    

    resources :pedidos

    resources :parametros    
    resources :produtos do
      resources :imagem_produtos
    end
    resources :categorias
    resources :banners    
    resources :usuarios
    resources :paginas
    resources :galerias do
      resources :imagem_galerias
    end
    
    
  end
  
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'home#index'
end
