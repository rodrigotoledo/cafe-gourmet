$(document).ready(function(){
  $('input[name*="valor"]').maskMoney({symbol:'R$',decimal:',',thousands:'.'});
  
  
  $('form .add_fields').click(function(event){
    time = new Date().getTime()
    regexp = new RegExp($(this).data('id'), 'g')
    console.log($(this).attr('data-fields'));
    var new_fields = $(this).data('fields').replace(regexp, time);
    $(this).before(new_fields);
    event.preventDefault()
  });
})
