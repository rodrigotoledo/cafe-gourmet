// function saveTag(width,height,position_y,position_x,tag_label,the_tag){
// 	var params = {'gallery_image_tagging':{'width':width,'height':height,'position_y':position_y,'position_x':position_x,'tag_label':tag_label}};
// 	$.post(gon.salvar_marcacao_path,params,function(id){
// 		the_tag.setId(id);
// 	});
// }
// 
// function removeTag(id){
//   $.post(gon.remover_marcacao_path,{'id':id});
// }
// 
// $(document).ready(function(){
// 	
// 	$(".to_tag").each(function(){
// 		$(this).tag({
// 			save: saveTag,
// 			remove: removeTag
// 		});
// 		
// 		var image_to_tag = $(this);
// 		$.getJSON(gon.marcacoes_path,{'id':image_to_tag.attr('image_id')},function(tags){
// 			$.each(tags, function(key,tag){
// 				console.log(tag)
// 				// console.log(image_to_tag.attr('image_id'))
// 				// console.log('.to_tag[image_id='+image_to_tag.attr('image_id')+']')
// 				// console.log($('img[image_id='+image_to_tag.attr('image_id')+']').attr('src'))
// 				$('.to_tag').addTag(tag.width,tag.height,tag.top,tag.left,tag.label,tag.id);
// 			});
// 		});
// 	})
// });


$(document).ready(function(){
	$('.photoTag').photoTag({
		requesTagstUrl: gon.marcacoes_path+'?id='+$('.photoTag').attr('image_id'), //'/static/photo-tag/tests/photo_tags/photo-tags.php',
		deleteTagsUrl: gon.remover_marcacao_path+'?id='+$('.photoTag').attr('image_id'), ///'/static/photo-tag/tests/photo_tags/delete.php',
		addTagUrl: gon.salvar_marcacao_path+'?id='+$('.photoTag').attr('image_id'), //'/static/photo-tag/tests/photo_tags/add-tag.php',
		showAddTagLinks: gon.pode_marcar,
		parametersForNewTag: {
			name: {
				parameterKey: 'name',
				isAutocomplete: true,
				autocompleteUrl: '/static/photo-tag/tests/photo_tags/names.php',
				label: 'Nome'
			}
		}
	});
});